package com.example.sqllite_agenda;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;


import com.example.sqllite_agenda.database.AgendaContactos;
import com.example.sqllite_agenda.database.Contacto;

import java.util.ArrayList;

public class ListActivity extends android.app.ListActivity {
    private MyArrayAdapter adapter;
    private ArrayList<Contacto> listaContacto;
private AgendaContactos agendaContacto;
private Button btnNuevo;
private void  llenarLista(){
    agendaContacto.openDataBase();
    listaContacto=agendaContacto.allContactos();
    agendaContacto.cerrar();
}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
btnNuevo = (Button)findViewById(R.id.btnNuevo);
agendaContacto = new AgendaContactos(this);

llenarLista();
adapter = new MyArrayAdapter(this,R.layout.layout_contacto,listaContacto);
String str = adapter.contactos.get(1).getNombre();
setListAdapter(adapter);

btnNuevo.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        finish();
    }
});
    }
    class MyArrayAdapter extends ArrayAdapter<Contacto>{
        private Context context;
        private int textViewResourceId;
        private ArrayList<Contacto> contactos;

        public MyArrayAdapter(@NonNull Context context, int resource, ArrayList<Contacto> contactos){
            super(context, resource);
            this.context = context;
            this.textViewResourceId = resource;
            this.contactos = contactos;
        }
        public View getView(final int position, View converView, ViewGroup viewGroup) {
            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewResourceId, null);
            TextView lblNombre = (TextView) view.findViewById(R.id.lblNombre);
            TextView lblTelefono = (TextView) view.findViewById(R.id.lbltel1);

            Button btnBorrar = (Button) findViewById(R.id.btnBorrar);
            Button btnModificar = (Button) findViewById(R.id.btnModificar);

            if (contactos.get(position).getFavorito() > 0) {
                lblNombre.setTextColor(Color.BLUE);
                lblTelefono.setTextColor(Color.BLUE);
            } else {
                lblNombre.setTextColor(Color.BLACK);
                lblTelefono.setTextColor(Color.BLACK);
            }
            lblNombre.setText(contactos.get(position).getNombre());
            lblTelefono.setText(contactos.get(position).getTelefono1());

           btnBorrar.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   agendaContacto.openDataBase();
                   agendaContacto.deleteContacto(contactos.get(position).get_ID());
                   agendaContacto.cerrar();
               }
           });
            btnModificar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("contacto", contactos.get(position));
                    Intent i = new Intent();
                    i.putExtras(bundle);
                    setResult(Activity.RESULT_OK,i);
                    finish();
                }
            });
            return view;
        }
    }

}

